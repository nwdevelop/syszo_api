<?php
class upload_image_webMod extends commonMod {
	public function index() {
		$data_return_array = array ();
		$files = $_FILES;
		if ($files ['file_path'] ['error'] == 1) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "2MBまでのJPEG,PNG,GIF画像を選択ください";
			$data_return_array ['data'] = "";
			
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
//		$ban_ext = array (
//				'php',
//				'asp',
//				'asp',
//				'html',
//				'htm',
//				'js',
//				'shtml',
//				'txt',
//				'aspx'
//		);
		$ban_ext=array('jpg','gif','png','jpeg');
		if (! empty ( $files )) {
			foreach ( $files as $file ) {
				$name = $file ['name'];
				$ext = explode ( '.', $file ['name'] );
				$ext = end ( $ext );
				$name = $ext [0];
				if(!in_array($ext, $ban_ext)){
					$data_return_array ['result'] = "0";
					$data_return_array ['msg'] = "2MBまでのJPEG,PNG,GIF画像を選択ください";
					$data_return_array ['data'] = "";
					
					$data_return = $this->JSON ( $data_return_array );
					die ( $data_return );
					exit ();
				}
			}
		} else {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "2MBまでのJPEG,PNG,GIF画像を選択ください";
			$data_return_array ['data'] = "";
			
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$init_name=$name;
		$name = hash ( 'sha256', $name );
		$str1 = substr ( $name, 0, 1 );
		$str2 = substr ( $name, 1, 1 );
		$str3 = substr ( $name, 2, 1 );
		// 文件路径
		$file_path = '/home/syszo_develop/public_html/www-t/ad/';
		// 文件URL路径
		$file_url = 'https://t.syszo.com/ad/';
		// 文件目录时间
		// $filetime='';
		$filetime = $str1 . '/' . $str2 . '/' . $str3;
		// var_dump($filetime);
		// 重命名
		function filename() {
			foreach ( $_FILES as $file ) {
				$name = explode ( '.', $file ['name'] );
				$ext = end ( $name );
				$name = $name [0];
			}
			$name = hash ( 'sha256', $name );
			$str1 = substr ( $name, 0, 1 );
			$str2 = substr ( $name, 1, 1 );
			$str3 = substr ( $name, 2, 1 );
			
			$name = substr ( $name, 3 );
			if (file_exists ( '/home/syszo_develop/public_html/www-t/ad/' . $str1 . '/' . $str2 . '/' . $str3 . '/' . $name . '.' . $ext )) {
				$rand = '-' . substr ( cp_uniqid (), - 5 );
			}
			return $name . $rand;
		}
		
		// 上传
		$upload = new UploadFile ();
		$upload->maxSize = 1024 * 1024 * 20; // 大小
//		$upload->allowExts = explode ( ',', $this->config ['ACCESSPRY_TYPE'] ); // 格式
		$upload->allowExts = $ban_ext; //格式
		$upload->savePath = $file_path . $filetime . '/'; // 保存路径
//		$upload->saveRule = 'filename'; // 重命名
		
		if (! $upload->upload ()) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "アップロードに失敗しました";
			$data_return_array ['data'] = "";

			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		} else {
			$info = $upload->getUploadFileInfo ();
			$info = $info [0];
			$ext = $info ['extension'];
			$file = $str1 . $str2 . $str3 . $info ['savename'];
			$title = str_replace ( '.' . $info ['extension'], '', $info ['name'] );

			// $data_return_array ['result'] = "1";
			// $data_return_array ['msg'] = "";
			// $url = $this->config['IMG_URL'].'media/images/'.$file;
			// $data_return_array ['data']['url'] = hash ( 'sha256', $url); // SHA265でハッシュ化

			$data_return_array ['result'] = "1";
			$data_return_array ['msg'] = "";
			$data_return_array ['data']['hurl'] = $file;
			$data_return_array ['data']['url'] = $file_url.$filetime."/".$info ['savename'];
			$data_return = $this->JSON ( $data_return_array );
			die ( "<script>document.domain = 't.syszo.com';</script>".$data_return );
			exit ();
		}
	}
	public function test() {
		$this->display ( "upload" );
	}
}
?>
