<?php
class loginMod extends commonMod {
	public function login() {
		if (empty ( $_POST['user_email'] ) || empty ( $_POST['user_pwd'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "パラメータが足りないです。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$user_info = $this->model->table ( "member" )->where ( "user_email = '" . $_POST['user_email'] . "' and status=0 " )->find ();
		//print_r($user_info);exit;
		if (! $user_info) {//判断登陆账户是否存在
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "メールアドレスまたはパスワードに誤りがあります。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if (md5($_POST ['user_pwd']) != $user_info ['user_pwd']) {//判断密码是否匹配
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "メールアドレスまたはパスワードに誤りがあります。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		//last login date
		$data = array (
				"last_login" =>time ()
		);
		$this->model->table ( "member" )->where ( "user_id = '" . $user_info['user_id'] . "' " )->data ( $data )->update ();

		$tmp['user_id']   = $user_info['user_id'];
		$tmp['user_nick'] = $user_info['user_nick'];
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function reg() {
		if (empty ( $_POST ['user_email'] ) || empty( $_POST ['user_nick'] ) || empty ( $_POST ['user_pwd'] ) ) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "全ての項目を入力してください。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if (strlen($_POST ['user_pwd'])<6) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "パスワードを6桁以上で入力してください。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$email_address = $_POST["user_email"];
        $pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9A-Za-z\\-_\\.]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
		if ( !preg_match( $pattern, $email_address ) ){//邮箱是否合法
	        $data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "メールアドレスが正しくありません。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
	    }
        $user_info = $this->model->table ( "member" )->where ( "user_email = '" . $_POST['user_email'] . "' or user_nick = '" . $_POST['user_nick'] . "' " )->find ();
		if ($user_info) {//判断注册的账户是否存在
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "該当ユーザーが既に存在しました。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data = array (
				"user_email" => $_POST ['user_email'],
				"user_nick"  => $_POST ['user_nick'],
				"user_pwd"   => md5($_POST ['user_pwd']),
				"insert_time"   => time ()
		);
		$id = $this->model->table ( "member" )->data ( $data )->insert ();
		$tmp['user_id'] = $id;
		
		//注册成功发送注册邮件
		ini_set("mbstring.internal_encoding","UTF-8");
		$to = $email_address;
		$subject = "【シス蔵】登録が完了しました。";
		$message = "こんにちは『シス蔵』です。\r\n\r\n";
		$message .= "メールアドレス: ".$email_address ."\r\n\r\n";
		$message .= "ニックネーム: ".$_POST ['user_nick'] ."\r\n";
		$from = "master@syszo.com";
		$headers = "From: $from";
		mb_language("uni"); 
		mb_send_mail($to,$subject,$message,$headers);

		//这个result flg简直没用啊..错误处理完全没有的样子..
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function tuisong(){
		$user_id 		= $_POST['user_id'];
		$device    		= $_POST['device'];
		$device_id	    = $_POST['device_id'];
		if (empty ( $user_id ) || empty ( $device ) || empty ( $device_id )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "パラメータが足りないです。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		/*$sql = " select * from app_member where device_id = '".$device_id."'";
		$user_info = $this->model->query($sql);
		 if($user_info){ //判断设备编号是否存在
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "該当ディバイスが存在しました。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		} */
		$data = array(
                "device"   		=> $device,
                "device_id"     => $device_id
		);
		$this->model->table("member")->where("user_id = '".$user_id."' ")->data($data)->update();
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function get_pwd(){
		$user_email = $_POST['user_email'];
		$data_return_array = array();
		if (empty ( $user_email )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "パラメータが足りないです。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$email = $this->model->table("member")->where("user_email = '".$user_email."' ")->data($data)->find();
		if(!$email){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "メールアドレスが存在しません。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
//		$data = array("user_pwd"   => "123456");
//		$new_pwd=rand(111111,999999);
		$new_pwd=substr(md5(rand(111111,999999)),0,6);
		$data = array("user_pwd"   => $new_pwd);
		$data['user_pwd'] = md5($data['user_pwd']);
		$email_info = $this->model->table("member")->where("user_email = '".$user_email."' ")->data($data)->update();
		$user_info = $this->model->table ( "member" )->where ( "user_email = '" . $user_email . "' " )->find ();
		if($email_info){//如果存在账户可以发送邮件重置密码
			ini_set("mbstring.internal_encoding","UTF-8");
			$to = $user_email;
//			$subject = "パスワード再設定";
			$subject = "【シス蔵】パスワード再発行";
//			$message = "パスワードを".$new_pwd."に設定しました。";
			$message = "こんにちは『シス蔵』です。\r\n\r\n";
			$message .= $user_info['user_nick']." さんのパスワードを再発行いたしました。\r\n\r\n";
			$message .= "新しいパスワード: ".$new_pwd ."\r\n";
//			$from = "app@ug-inc.net";
			$from = "master@syszo.com";
			$headers = "From: $from";
			mb_language("uni"); 
			mb_send_mail($to,$subject,$message,$headers);
			
			$data_return_array ['result'] = "1";
			$data_return_array ['msg'] = "";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
	}
	
	public function chenge_pwd(){
		$list = $this->model->table("member")->select();
		$i = 0;
		foreach ($list as $v){
			$data['user_pwd'] = md5($v['user_pwd']);
			$this->model->table('member')->data($data)->where("user_id = ".$v['user_id'])->update();
			$i++;
		}
		echo $i;
	}
}













