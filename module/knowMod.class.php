<?php
class knowMod extends commonMod {
	//知恵袋列表接口
	public function info_list() {
		$p_num = $_POST ['p_num'];
		$p_size = $_POST ['p_size'];
		$user_id = $_POST ['user_id'];
		if (empty ( $p_size )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if (empty ( $p_num )) {//分页默认为第一页
			$p_num = 1;
		}
		if (empty ( $_POST ['type'] )) {//排序类型默认第一种发布日期排序
			$_POST ['type'] = 1;
		}
		$str = ($p_num - 1) * $p_size;
		if ($_POST ['type'] == '1') {//按照发布日期排序
			$list = $this->model->table ( "know" )->limit ( "$str" . "," . "$p_size" )->order ( "insert_time desc" )->select ();
			if ($list) {
				foreach ( $list as $k => $vo ) {
					$count = $this->model->table ( "know_comments" )->where ( "info_id = '" . $vo ['id'] . "' " )->count ();//计算回复数量
					$list [$k] ['c_n'] = $count;
					unset ( $count );
				}
			}
		} elseif ($_POST ['type'] == '2') {//按照最新回复排序
			$list = $this->model->table ( "know" )->where ( "comments_time !=0 " )->limit ( "$str" . "," . "$p_size" )->order ( "comments_time desc , insert_time desc" )->select ();
			if ($list) {
				foreach ( $list as $k => $vo ) {
					//$count = $this->model->table ( "know_comments" )->where ( "info_id = '" . $vo ['id'] . "' " )->count ();
					//按coment排序所以显示最终coment时间
					$list [$k] ['insert_time'] = $list [$k] ['comments_time'];
					$list [$k] ['c_n'] = $vo['comments_sum'];//回复数量
					//unset ( $count );
				}
//				$list = $this->sortByMultiCols ( $list, array (
//						'c_n' => SORT_DESC 
//				) );
			}
		} elseif ($_POST ['type'] == '3') {
			$id = $this->model->table ( "know_comments" )->where ( "user_id = '" . $user_id . "' " )->select ();
			$idn = $this->model->table ( "know" )->where ( "user_id = '" . $user_id . "' " )->select ();
			$ids = array ();
			if ($id||$idn) {
				if ($id) {
					foreach ( $id as $val ) {//取出user_id为自己的文章id
						$ids [] = $val ['info_id'];
					}
				}
				if ($idn) {
					foreach ( $idn as $valn ) {//取出user_id为自己的文章id
						$ids [] = $valn ['id'];
					}
				}

				$ids = array_unique ( $ids );//去掉重复id
				$ids = implode ( ",", $ids );//以，分割成字符串

				$list = $this->model->table ( "know" )->where ( "id in (" . $ids . ") " )->limit ( "$str" . "," . "$p_size" )->order ( "create_time desc , insert_time desc" )->select ();//查出自己发布的文章信息
				
			if ($list) {//遍历我的文章id查出我的文章回复数量
				foreach ( $list as $k => $vo ) {
					$count = $this->model->table ( "know_comments" )->where ( "info_id = '" . $vo ['id'] . "' " )->count ();
					//myk 我的文章显示我回复的日期
					$comentd =$this->model->table ( "know_comments" )->where ( "info_id = '" . $vo ['id'] . "' and user_id = '" . $user_id . "' " )->limit ( "0,1" )->order ( "insert_time desc" )->find ();
					if ($comentd) {
						$list [$k] ['insert_time'] = $comentd ['insert_time'];
					}
					//myk end
					$list [$k] ['c_n'] = $count;
					unset ( $count );
				}
				
			}
			} else {
				$list = array ();
			}
		}elseif ($_POST['type'] == '4'){//按照是否加急排序1不加急，2加急
			//$list = $this->model->table ( "know" )->limit ( "$str" . "," . "$p_size" )->order ( "urgent desc,insert_time desc" )->select ();//按照加急和时间排序
			$list = $this->model->table ( "know" )->where("urgent = 2 and insert_time >=".strtotime("-36 hour"))->limit ( "$str" . "," . "$p_size" )->order ( "urgent desc,insert_time desc" )->select ();
			if ($list) {
				foreach ( $list as $k => $vo ) {
					$count = $this->model->table ( "know_comments" )->where ( "info_id = '" . $vo ['id'] . "' " )->count ();//查找文章回复数量
					$list [$k] ['c_n'] = $count;
					unset ( $count );
				}
			}
		}
		
		$tmp = array ();
		if ($list) {
			//コメント時間でソート
			//usort($list, function ($a, $b) {
			//if ($a['insert_time'] == $b['insert_time']) return 0;
			//	return ($a['insert_time'] > $b['insert_time']) ? -1 : 1;
			//});

			foreach ( $list as $key => $val ) {//遍历查出来的文章信息
				$tmp [$key] ['know_id'] = $val ['id'];
				$tmp [$key] ['title'] = $val ['title'];
				$tmp [$key] ['comment_sum'] = $val ['c_n'];
				$tmp [$key] ['urgent'] = $val ['urgent'];
				$tmp [$key] ['time'] = date ( "Y-m-d H:i", $val ['insert_time'] );
				$tmp [$key] ['good_sum'] = $this->model->table ( "good" )->where (" info_id =  '" . $val ['id'] . "' and c_id = '0' and type = '1' " )->count ();
				if($user_id){
				$c = $this->model->table ( "know_comments" )->where ( "user_id = '" . $user_id . "' and info_id = '" . $val ['id'] . "' " )->find ();
				if ($c) {
					$tmp [$key] ['comments'] = '2';//评论过
				} else {
					$tmp [$key] ['comments'] = '1';//没评论过
				}
				unset ( $c );
				}else{
					$tmp [$key] ['comments'] = '1';//没评论过
				}
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//知恵袋详情
	public function info() {
		$user_id = $_POST ['user_id'];
		$know_id = $_POST ['know_id'];
		if (empty ( $know_id )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if($_POST ['user_id']){//如果有用户登录，则点赞按照点赞表是否点赞过区分。
			$good = $this->model->table ( "good" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $know_id . "' and c_id = '0' and type = '1' " )->find ();
			if ($good) {
				$data['if_good'] = "2";//赞过
			} else {
				$data['if_good'] = "1";//没赞过
			}
		}else{
			$data['if_good'] = "1";//如果没有登录用户，默认没赞过
		}
		$info = $this->model->table ( "know" )->where ( "id = '" . $know_id . "' " )->find ();//查出文章信息
		//print_r($info);exit;
		if ($info) {
			$user = $this->model->table ( "member" )->where ( "user_id = '" . $info ['user_id'] . "' " )->find ();//通过member表查找用户昵称
		}
		$data ['title'] = $info ['title'];
		$data ['user_id'] = $info ['user_id'];
		$data ['vip_flg'] = $this->if_vip($info ['user_id']);   //0:普通用户,1:企业用户(VIP)
		$data ['user_name'] = $user ['user_nick'];
		$data ['time'] = date ( "Y-m-d H:i:s", $info ['insert_time'] );
		//$data ['content'] = $info ['content'];
//		$info ['content'] = str_replace("\\","\\\\",$info ['content']);
		$tmp_data = str_replace("\r", "\\r", $info ['content']);
		$data ['content'] = str_replace("\n", "\\n", $tmp_data);
		$data ['comment_sum'] = $this->model->table ( "know_comments" )->where ( "  info_id = '".$know_id."' " )->count ();
		$data ['info_img'] = $info['url'];
		$data ['status'] = $user['status'];
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $data;
		$data_return = $this->JSON ( $data_return_array );
		$data_return = str_replace("	","    ",$data_return);
//		$data_return = str_replace("\\","\\\\",$data_return);
//		$data_return = str_replace("\\\\r", "\\r", $data_return);
//		$data_return = str_replace("\\\\n", "\\n", $data_return);
		print_r($data_return);exit;
		die ( $data_return );
		exit ();
	}
	//知恵袋评论列表接口
	public function comments_list() {
		$know_id = $_POST ['know_id'];
		$p_size = $_POST ['p_size'];
		$p_num = $_POST ['p_num'];
		if (empty ( $know_id ) ||  empty ( $p_size )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if (empty ( $p_num )) {//默认分页为第一页
			$p_num = 1;
		}
		$str = ($p_num - 1) * $p_size;
		$list = $this->model->table ( "know_comments" )->limit ( "$str" . "," . "$p_size" )->where ( " info_id = '" . $know_id . "' " )->order ( "insert_time asc" )->select ();//查找该文章的所有评论
		
		$tmp = array ();
		if ($list) {
			foreach ( $list as $key => $val ) {
				$tmp [$key] ['user_id'] = $val ['user_id'];
				$tmp [$key] ['vip_flg'] = $this->if_vip($val ['user_id']);   //0:普通用户,1:企业用户(VIP)
				$user = $this->model->table ( "member" )->where ( "user_id  = '" . $val ['user_id'] . "' " )->find ();//通过member表查出用户昵称
				$tmp [$key] ['user_name'] = $user ['user_nick'];
				$tmp [$key] ['comments_id'] = $val ['id'];
				$tmp [$key] ['comments_time'] = date ( "Y-m-d H:i", $val ['insert_time'] );
				//$tmp [$key] ['comments'] = $val ['content'];
//				$val ['content'] = str_replace("\\", "\\\\", $val ['content']);
				$tmp_data = str_replace("\r", "\\r", $val ['content']);
				$tmp [$key] ['comments']  = str_replace("\n", "\\n", $tmp_data);
				if($_POST['user_id']){//如果有用户登录，则点赞按照点赞表是否点赞过区分。
				$good = $this->model->table ( "good" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $know_id . "' and c_id = '" . $val ['id'] . "' and type = '1' " )->find ();
					if ($good) {
						$tmp [$key] ['if_good'] = "2";//赞过
					} else {
						$tmp [$key] ['if_good'] = "1";//没赞过
					}
				}else{
					$tmp [$key] ['if_good'] = "1";//如果没有登录用户，默认没赞过
				}

				//コメントのいいね　数量を取得する
				$comment_goods = $this->model->table ( "good" )->where ( "info_id =  '" . $know_id . "' and c_id = '" . $val ['id'] . "' and type = '1' " )->count ();
					$tmp [$key] ['comment_goods'] = $comment_goods;//该评论的点赞数

				
				if($_POST['user_id']){//如果有用户登录，判断是否评论过（这个干啥用的？）。
				$if_comment = $this->model->table ( "know_comments" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $know_id . "' " )->find ();
					if ($if_comment) {
						$tmp [$key] ['if_comment'] = "2";//评论过
					} else {
						$tmp [$key] ['if_comment'] = "1";//没评论过
					}
					}else{
						$tmp [$key] ['if_comment'] = "1";//如果没有登录用户，默认没评论过
				}
				
				if($_POST['user_id']){//如果有用户登录，则是否为自己的信息通过know表查询文章id和用户user_id是否存在区分是否为自己发布的文章
				$if_myself = $this->model->table ( "know" )->where ( "id  = '" . $val ['info_id'] . "' and user_id  = '" . $_POST ['user_id'] . "' " )->find ();
					if ($if_myself) {
						$tmp [$key] ['if_myself'] = "2";//是
					} else {
						$tmp [$key] ['if_myself'] = "1";//不是
					}
					}else{
					$tmp [$key] ['if_myself'] = "1";//如果没有登录用户，默认不是自己发布的文章
				}
			}
		}
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		$data_return = str_replace("	","    ",$data_return);
//		$data_return = str_replace("\\","\\\\",$data_return);
//		$data_return = str_replace("\\\\r", "\\r", $data_return);
//		$data_return = str_replace("\\\\n", "\\n", $data_return);
		die ( $data_return );
		exit ();
	}
	//评论编辑
	public function comments_edit() {
		if (empty ( $_POST['user_id'] ) || empty ( $_POST['info_id'] ) || empty ( $_POST['content'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		//查出该用户的所有评论
		$list = $this->model->table ( "know_comments" )->where(" user_id = '".$_POST['user_id']."' ")->find ();
		
		if ($list) {//如果存在自己的评论
			$data = array (
					"content" => $_POST['content'],
					"insert_time" => time()
			);
			//对这篇文章自己的评论进行修改操作
			$this->model->table ( 'know_comments' )->data ( $data )->where (" user_id = '".$_POST['user_id']."' and id = '".$_POST['info_id']."' ")->update ();
		}else{
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "該当コメントがありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//评论删除
	public function comments_del() {
		if (empty ( $_POST['user_id'] ) || empty ( $_POST['info_id'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		//查出该文章user_id的所有评论
		$list = $this->model->table ( "know_comments" )->where(" user_id = '".$_POST['user_id']."' and id = '".$_POST['info_id']."' ")->find ();
		
		if ($list) {
			//如果存在进行评论删除操作
			$com_id = $this->model->table ( 'know_comments' )->where (" user_id = '".$_POST['user_id']."' and id = '".$_POST['info_id']."' ")->delete ();
			
			if($com_id){//如果删除操作成功
				//查出know表的文章评论数字段comments_sum
				$com_sum = $this->model->table('know')->where("id = '".$list['info_id']."' ")->find();
				//删除评论，减 1
				$data = array (
						"comments_sum" => $com_sum['comments_sum']-"1",
				);
				//评论数量更新到数据库
				$this->model->table('know')->where("id = '".$list['info_id']."' ")->data($data)->update();
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//知恵袋评论接口
	public function comments() {
		$user_id = $_POST ['user_id'];
		$know_id = $_POST ['know_id'];
		$reply_content = $_POST ['reply_content'];
		
		if (empty ( $user_id ) || empty ( $know_id ) || empty ( $reply_content )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		
		$data = array (
				"user_id" => $user_id,
				"info_id" => $know_id,
				"content" => $reply_content,
				"insert_time" => time () 
		);
		//评论数据插入到评论表
		$com_id = $this->model->table ( "know_comments" )->data ( $data )->insert ();
		if($com_id){//如果插入成功
			//查询文章know表comments_sum字段
			$com_sum = $this->model->table('know')->where("id = $know_id")->find();
			$data = array (
									"comments_sum" => $com_sum['comments_sum']+"1",//评论数量+1
									"comments_time" => time (),//评论时间
									"create_time" => time ()//评论时间
			);
			//更新到数据库
			$this->model->table('know')->where("id = $know_id")->data($data)->update();

			//记录该用户最后的评论时间到用户表
			//last_input
			$data = array (
			"last_input" =>time ()
			);
			$this->model->table ( "member" )->where ( "user_id = '" . $user_id . "' " )->data ( $data )->update ();

			$user_info = $this->model->table ("member" )->where("user_id = " . $com_sum['user_id'] . " " )->find ();

			if($user_info){//如果存在账户可以发送邮件
				ini_set("mbstring.internal_encoding","UTF-8");
				$user_email=$user_info['user_email'];
				$to = $user_email;
				$subject = "【シス蔵】あなたの投稿にコメントがありました。";
				$message = "こんにちは『シス蔵』です。\r\n\r\n";
				$message .= "あなたの投稿にコメントがありました。\r\n\r\n";
				$message .= "→ http://syszo.com/detail.php?id=".$know_id."\r\n\r\n";
				$from = "master@syszo.com";
				$headers = "From: $from";
				mb_language("uni"); 
				mb_send_mail($to,$subject,$message,$headers);
				
				$data_return_array ['result'] = "1";
				$data_return_array ['msg'] = "";
				$data_return_array ['data'] = "";
				$data_return = $this->JSON ( $data_return_array );
				die ( $data_return );
				exit ();
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//知恵袋点赞
	public function good() {
		if (empty ( $_POST ['know_id'] ) || empty ( $_POST ['user_id'] ) || !isset ( $_POST ['comments_id'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		//查询是否赞过
		$good = $this->model->table ( "good" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $_POST ['know_id'] . "' and c_id = '" . $_POST ['comments_id'] . "' and type = '1'" )->find ();
		if ($good) {//如果赞过，再次出发进行取消点赞
			$this->model->table ( "good" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $_POST ['know_id'] . "' and c_id = '" . $_POST ['comments_id'] . "' and type = '1'" )->delete ();
		} else {//如果没有赞过进行插入操作
			$data = array (
					"user_id" => $_POST ['user_id'],
					"info_id" => $_POST ['know_id'],
					"c_id" => $_POST ['comments_id'],
					"insert_time" => time (),
					"type" => "1"
			);
			$this->model->table ( "good" )->data ( $data )->insert ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//帖子赞数（评论的赞数在评论列表里取得comments_list $comment_goods
	public function get_good_count() {
		if (empty ( $_POST ['know_id'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$good_count = 0;//知恵袋点赞
		$good_count = $this->model->table ( 'good' )->where ( "info_id in (".$_POST ['know_id'].")  and c_id = '0'  and type = 1" )->count ();
		$tmp ['good_count'] = $good_count;
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//首页检索知恵袋关键字列表
	public function search_index() {
		$p_num = $_POST ['p_num'];
		$rank_times = date('Ym', time()).'00';
		
		
		if (empty ( $p_num )) {//分页默认第一页
			$p_num = 1;
		}
		$str = ($p_num - 1) * 10;//显示十条数据
		//按照查找次数排序列出前10个关键字
		$list = $this->model->table ( "keyword" )->where ( "rank_times = '" . $rank_times . "' " )->limit ( "$str" . "," . "10" )->order ( "count desc" )->select ();

		$data_return_array ['result'] = "0";

		if ($list) {//如果存在数据进行遍历
			foreach ( $list as $k => $v ) {
				$tmp [$k] ['keyword'] = $v ['keyword'];
			}
		$data_return_array ['result'] = "1";
		}
	//	$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
	//	$data_return_array ['msg'] = $rank_times;
		$data_return_array ['data'] ['time'] = date ( "Y-m-d H:i" );
		$data_return_array ['data'] ['list'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//检索知恵袋列表
	public function search() {
		$p_size = $_POST ['p_size'];
		$p_num = $_POST ['p_num'];
		$user_id = $_POST ['user_id'];
		if (empty ( $p_size ) || empty ( $_POST ['keyword'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない!";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if (empty ( $p_num )) {//分页默认第一页
			$p_num = 1;
		}
		if ($_POST ['keyword']) {//如果keyword有值
			$where = "title like '%" . $_POST ['keyword'] . "%' or content like '%" . $_POST ['keyword'] . "%' ";
		} else {
			$where = '1';
		}
		$str = ($p_num - 1) * $p_size;
		//查出文章
		$list = $this->model->table ( "know" )->where ( $where )->limit ( "$str" . "," . "$p_size" )->order ( "insert_time desc" )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $key => $val ) {//遍历文章信息
				$tmp [$key] ['know_id'] = $val ['id'];
				$tmp [$key] ['title'] = $val ['title'];
				$tmp [$key] ['comment_sum'] = $this->model->table ( "know_comments" )->where ( " info_id = '" . $val ['id'] . "' " )->count ();
				$tmp [$key] ['time'] = date ( "Y-m-d H:i", $val ['insert_time'] );
				$c = $this->model->table ( "know_comments" )->where ( "user_id = '" . $user_id . "' and info_id = '" . $val ['id'] . "' " )->find ();
				if ($c) {//是否评论过
					$tmp [$key] ['comments'] = '2';
				} else {
					$tmp [$key] ['comments'] = '1';
				}
				unset ( $c );
			}

			//仅当有查询结果时才更新关键字计量 2015/10/02 修改
			$count = 1;//搜索次数记录默认为1
			$rank_times = date('Ym', time()).'00';
			//$k = $this->model->table ( "keyword" )->where ( "keyword = '" . $_POST ['keyword'] . "' " )->find ();
			$k = $this->model->table ( "keyword" )->where ( "keyword = '" . $_POST ['keyword'] . "' and rank_times = '" . $rank_times . "' ")->find ();
			
			if ($k) {//如果关键词搜索过，进行数量+1
				$data = array (
						"count" => $k ['count'] + 1 
				);
				//$this->model->table ( 'keyword' )->data ( $data )->where ( "keyword = '" . $_POST ['keyword'] . "' " )->update ();
				$this->model->table ( 'keyword' )->data ( $data )->where ( "keyword = '" . $_POST ['keyword'] . "' and rank_times = '" . $rank_times . "' " )->update ();
			} else {//如果关键词没有搜索过，默认为1
				$data = array (
						"rank_times" => $rank_times,
						"keyword" => $_POST ['keyword'],
						"count" => $count
				);
				$this->model->table ( "keyword" )->data ( $data )->insert ();
			}
		//$list结束
		}
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] ['time'] = "";
		$data_return_array ['data'] ['list'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//知恵袋发布
	public function add() {
		if (empty ( $_POST ['user_id'] ) || empty ( $_POST ['title'] ) || empty ( $_POST ['content'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		
		$data = array (
				"url" => $_POST ['img'],
				"title" => $_POST ['title'],
				"insert_time" => time (),
				"create_time" => time (),
				"content" => $_POST ['content'],
				"user_id" => $_POST ['user_id'],
				"urgent" => $_POST ['urgent'] 
		);
		$this->model->table ( "know" )->data ( $data )->insert ();
			//记录该用户最后的评论时间到用户表
			//last_input
			$data = array (
					"last_input" =>time ()
			);
			$this->model->table ( "member" )->where ( "user_id = '" . $_POST ['user_id'] . "' " )->data ( $data )->update ();

		//メール送信
		$title=$_POST['title'];
		$content=$_POST['content'];
		$user_id=$_POST['user_id'];

		$know_info = $this->model->table ( "know" )->where ( "title = '" . $title . "' and user_id = $user_id" )->find ();
		$know_id = $know_info['id'];
		$user_info = $this->model->table ( "member" )->where ( "user_id = $user_id")->find ();
		$user_nick = $user_info['user_nick'];

		ini_set("mbstring.internal_encoding","UTF-8");
		$from = "master@syszo.com";
		//$subject = mb_encode_mimeheader("【シス蔵】 投稿「".$title."」が投稿されました");
		$subject = "【シス蔵】 投稿「".$title."」が投稿されました";
		$headers = "From: $from";
		$message = "";
		$message .= "こんにちは『シス蔵』です。\r\n\r\n";
		$message .= $user_nick." さんより投稿がありました。\r\n\r\n";
		if ($know_id!="") {
			$message .= "この投稿にコメントをしていただける場合は、下記URLへアクセスしてください。\r\n";
			//$message .= "※下記URLはログイン情報を含んでいます。取扱にご注意ください。\r\n\r\n";
			$message .= "→ http://syszo.com/detail.php?id=".$know_id."\r\n\r\n\r\n";
		}
		$message .= "----------投稿内容ここから\r\n";
		$message .= $content."\r\n";
		$message .= "----------投稿内容ここまで\r\n\r\n\r\n";
		$message .= "▼配信停止のお手続き\r\n\r\n";
		$message .= "・新着メールの配信停止やメールアドレスの変更等、ご登録内容の変更は、\r\n";
		$message .= "　下記URLからログイン後、マイページの「編集」にてご設定くだ さい。\r\n\r\n";
		$message .= "───────────────\r\n";
		$message .= "『シス蔵』　http://syszo.com/\r\n";
		$message .= "───────────────\r\n\r\n";

		$list = $this->model->table ( "member" )->where("mail_flg = 1")->select ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$to = $vo['user_email'];
				mb_language("uni"); 
				//mail($to,$subject,$message,$headers);
				mb_send_mail($to,$subject,$message,$headers);
			}
		}
		if($_POST['urgent']=="2"){
			$list = $this->model->table ( "member" )->where("mail_flg = 2")->select ();
			if ($list) {
				foreach ( $list as $k => $vo ) {
					$to = $vo['user_email'];
					mb_language("uni"); 
					//mail($to,$subject,$message,$headers);
					mb_send_mail($to,$subject,$message,$headers);
				}
			}
		}

		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//知恵袋编辑
	public function edit() {
		if (empty ( $_POST ['user_id'] ) || empty ( $_POST ['title'] ) || empty ( $_POST ['content'] ) || empty($_POST['know_id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
	
		$data = array (
				"url" => $_POST ['img'],
				"title" => $_POST ['title'],
				"insert_time" => time (),
				"content" => $_POST ['content'],
				"user_id" => $_POST ['user_id'],
				"urgent" => $_POST ['urgent']
		);
		$this->model->table ( "know" )->where("user_id = '".$_POST ['user_id']."' and id = '".$_POST['know_id']."' ")->data ( $data )->update ();
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//知恵袋删除
	public function del(){
		$user_id = $_POST['user_id'];
		$know_id = $_POST['know_id'];
		if (empty ( $user_id ) || empty($know_id)) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$this->model->table('know')->where(" id = $know_id and user_id = $user_id")->delete();
		$this->model->table('know_comments')->where(" info_id = $know_id")->delete();
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	//全ての新着
	public function send_noti_all(){
		$title = $_POST['title'];
		$content = $_POST['content'];
		$user_id = $_POST['user_id'];

		$know_info = $this->model->table ( "know" )->where ( "title = '" . $title . "' and user_id = $user_id" )->find ();
		$know_id = $know_info['id'];
		$user_info = $this->model->table ( "member" )->where ( "user_id = $user_id")->find ();
		$user_nick = $user_info['user_nick'];

		ini_set("mbstring.internal_encoding","UTF-8");
		$from = "master@syszo.com";
		//$subject = mb_encode_mimeheader("【シス蔵】 投稿「".$title."」が投稿されました");
		$subject = "【シス蔵】 投稿「".$title."」が投稿されました";
		$headers = "From: $from";
		$message = "";
		$message .= "こんにちは『シス蔵』です。\r\n\r\n";
		$message .= $user_nick." さんより投稿がありました。\r\n\r\n";
		if ($know_id!="") {
			$message .= "この投稿にコメントをしていただける場合は、下記URLへアクセスしてください。\r\n";
			//$message .= "※下記URLはログイン情報を含んでいます。取扱にご注意ください。\r\n\r\n";
			$message .= "→ http://syszo.com/detail.php?id=".$know_id."\r\n\r\n\r\n";
		}
		$message .= "----------投稿内容ここから\r\n";
		$message .= $content."\r\n";
		$message .= "----------投稿内容ここまで\r\n\r\n\r\n";
		$message .= "▼配信停止のお手続き\r\n\r\n";
		$message .= "・新着メールの配信停止やメールアドレスの変更等、ご登録内容の変更は、\r\n";
		$message .= "　下記URLからログイン後、マイページの「編集」にてご設定くだ さい。\r\n\r\n";
		$message .= "───────────────\r\n";
		$message .= "『シス蔵』　http://syszo.com/\r\n";
		$message .= "───────────────\r\n\r\n";

		$list = $this->model->table ( "member" )->where("mail_flg = 1")->select ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$to = $vo['user_email'];
				mb_language("uni"); 
				//mail($to,$subject,$message,$headers);
				mb_send_mail($to,$subject,$message,$headers);
			}
		}
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//トラブル発生中のみ
	public function send_noti_urgent(){
		$title = $_POST['title'];
		$content = $_POST['content'];
		$user_id = $_POST['user_id'];

		$know_info = $this->model->table ( "know" )->where ( "title = '" . $title . "' and user_id = $user_id" )->find ();
		$know_id = $know_info['id'];
		$user_info = $this->model->table ( "member" )->where ( "user_id = $user_id")->find ();
		$user_nick = $user_info['user_nick'];

		ini_set("mbstring.internal_encoding","UTF-8");
		$from = "master@syszo.com";
		//$subject = mb_encode_mimeheader("【シス蔵】 投稿「".$title."」が投稿されました");
		$subject = "【シス蔵】 投稿「".$title."」が投稿されました";
		$headers = "From: $from";
		$message = "";
		$message .= "こんにちは『シス蔵』です。\r\n\r\n";
		$message .= $user_nick." さんより投稿がありました。\r\n\r\n";
		if ($know_id!="") {
			$message .= "この投稿にコメントをしていただける場合は、下記URLへアクセスしてください。\r\n";
			//$message .= "※下記URLはログイン情報を含んでいます。取扱にご注意ください。\r\n\r\n";
			$message .= "→ http://syszo.com/detail.php?id=".$know_id."\r\n\r\n\r\n";
		}
		$message .= "----------投稿内容ここから\r\n";
		$message .= $content."\r\n";
		$message .= "----------投稿内容ここまで\r\n\r\n\r\n";
		$message .= "▼配信停止のお手続き\r\n\r\n";
		$message .= "・新着メールの配信停止やメールアドレスの変更等、ご登録内容の変更は、\r\n";
		$message .= "　下記URLからログイン後、マイページの「編集」にてご設定くだ さい。\r\n\r\n";
		$message .= "───────────────\r\n";
		$message .= "『シス蔵』　http://syszo.com/\r\n";
		$message .= "───────────────\r\n\r\n";

		$list = $this->model->table ( "member" )->where("mail_flg = 2")->select ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$to = $vo['user_email'];
				mb_language("uni"); 
				//mail($to,$subject,$message,$headers);
				mb_send_mail($to,$subject,$message,$headers);
			}
		}
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
}