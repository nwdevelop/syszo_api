<?php
class menuMod extends commonMod {
	public function get_msg_cnt() {
		$user_id = $_POST ['user_id'];
		if (empty ( $user_id ) ) {
			$data_return_array ['result'] = "1";
			$data_return_array ['msg'] = "";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$notice_time_cnt=$this->model->table ( "notice_time" )->where ( "  user_id = '".$user_id."' " )->count ();
		if(empty ( $notice_time_cnt )){
			$db_data = array (
					"user_id" => $user_id,
					"notice_time_know" => time (),
					"notice_time_bird" => time (),
					"notice_time_friend" => time (),
					"notice_time_msg" => time ()
			);
			$this->model->table ( "notice_time" )->data ( $db_data )->insert ();
			$notice_time_know=time();
			$notice_time_bird=time();
			$notice_time_friend=time();
			$notice_time_msg=time();
		}else{
			$notice_time_info = $this->model->table ( "notice_time" )->where ( "user_id = '" . $user_id . "' " )->find ();
			$notice_time_know=$notice_time_info['notice_time_know'];
			$notice_time_bird=$notice_time_info['notice_time_bird'];
			$notice_time_friend=$notice_time_info['notice_time_friend'];
			$notice_time_msg=$notice_time_info['notice_time_msg'];
//			$db_data = array (
//					"user_id" => $user_id,
//					"update_time" => time ()
//			);
//			$this->model->table('notice_time')->where("user_id = $user_id")->data($db_data)->update();
		}
		//1.知恵袋・・・新規投稿があがったら①、2個あがったら②、3個あがったら③
		$data['know_msg_cnt'] = $this->model->table ( "know" )->where ( " insert_time > " .$notice_time_know  ." AND user_id <> ". $user_id)->count ();
		//2.シスッター・・・自分のつぶやきに対するコメント、自分のコメントに対するコメントがあったら①、②、③と数字つける。
		$id = $this->model->table ( "brid" )->where ( "user_id = '" . $user_id . "' " )->select ();
		$ids = array ();
		if ($id) {
			foreach ( $id as $val ) {
				$ids [] = $val ['id'];
			}
		}
		$ids = array_unique ( $ids );
		$my_bird_ids = implode ( ",", $ids );
		if(!empty($my_bird_ids)){
			$data['brid_comments_msg_cnt'] = $this->model->table ( "brid_comments" )->where ( "bird_id in (" . $my_bird_ids . ") AND insert_time > " .$notice_time_bird ." AND user_id <> ". $user_id)->count ();
		}else{
			$data['brid_comments_msg_cnt'] = 0;
		}
		//3.シストモ・・・新規フォロワーがあったら①、2人だったら②
		$data['follow_msg_cnt'] = $this->model->table ( "fans" )->where ( "  user_id = '".$user_id."' AND insert_time > " .$notice_time_friend )->count ();
		//4.メッセンジャー・・・新規メッセージがあったら①、2個あがったら②、3個あがったら③
		$data['message_msg_cnt'] = $this->model->table ( "group_info" )->where ( "  rec_id = '".$user_id."' AND insert_time > " .$notice_time_msg  )->count ();
//		//5.上記の4つの中の何か1つでも新着があればホーム画面のAPPアイコンバッチに①をつける。2個目3個目はいらない。何個新着があっても①で表示。
//		$data['new_msg_cnt'] = $data['know_msg_cnt']+$data['brid_comments_msg_cnt']+$data['follow_msg_cnt']+$data['message_msg_cnt'];

		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $data;
		$data_return = $this->JSON ( $data_return_array );
		print_r($data_return);exit;
		die ( $data_return );
		exit ();
	}
	public function set_msg_read_time() {
		$user_id = $_POST ['user_id'];
		$menu_flg = $_POST ['menu_flg'];
		if (empty ( $user_id ) ) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "システムエラー";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$notice_time_cnt=$this->model->table ( "notice_time" )->where ( "  user_id = '".$user_id."' " )->count ();
		if(empty ( $notice_time_cnt )){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "システムエラー";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}else{
			if($menu_flg=='1'){//1.知恵袋
				$db_data = array (
						"user_id" => $user_id,
						"notice_time_know" => time ()
				);
				$this->model->table('notice_time')->where("user_id = $user_id")->data($db_data)->update();
			}elseif($menu_flg=='2'){//2.シスッター
				$db_data = array (
						"user_id" => $user_id,
						"notice_time_bird" => time ()
				);
				$this->model->table('notice_time')->where("user_id = $user_id")->data($db_data)->update();
			}elseif($menu_flg=='3'){//3.シストモ
				$db_data = array (
						"user_id" => $user_id,
						"notice_time_friend" => time ()
				);
				$this->model->table('notice_time')->where("user_id = $user_id")->data($db_data)->update();
			}elseif($menu_flg=='4'){//4.メッセンジャー
				$db_data = array (
						"user_id" => $user_id,
						"notice_time_msg" => time ()
				);
				$this->model->table('notice_time')->where("user_id = $user_id")->data($db_data)->update();
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		print_r($data_return);exit;
		die ( $data_return );
		exit ();
	}
}