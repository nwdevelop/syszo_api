<?php

class commonMod {
	protected $model = NULL; // 数据库模型
	protected $layout = NULL; // 布局视图
	protected $config = array ();
	private $_data = array ();
	protected $mac_sign = NULL;
	protected $openid = NULL;
	
	
	protected function init() {
	}
	public function __construct() {
		global $config;
		$this->config = $config;
		
		$this->mac_sign = $_GET['sign'];  //获取加密后的mac地址
		$this->openid = $_GET['openid'];  //获取加密后的mac地址
		
		define("WEB_URL_SYSTEM", $this->config['WEB_URL_SYSTEM']);
		
		$this->model = self::initModel ( $this->config );
		$this->init ();
		
		
		if(!empty($_POST['user_token'])){
			self::check_token($_POST['user_token']);
		}
		
	}
	
	// 初始化模型
	static public function initModel($config) {
		static $model = NULL;
		if (empty ( $model )) {
			$model = new cpModel ( $config );
		}
		return $model;
	}
	public function __get($name) {
		return isset ( $this->_data [$name] ) ? $this->_data [$name] : NULL;
	}
	public function __set($name, $value) {
		$this->_data [$name] = $value;
	}
	
	// 获取模板对象
	public function view() {
		static $view = NULL;
		if (empty ( $view )) {
			$view = new cpTemplate ( $this->config );
		}
		return $view;
	}
	
	// 模板赋值
	protected function assign($name, $value) {
		return $this->view ()->assign ( $name, $value );
	}
	
	// 模板显示
	protected function display($tpl = '', $return = false, $is_tpl = true) {
		if ($is_tpl) {
			$tpl = empty ( $tpl ) ? $_GET ['_module'] . '_' . $_GET ['_action'] : $tpl;
			if ($is_tpl && $this->layout) {
				$this->__template_file = $tpl;
				$tpl = $this->layout;
			}
		}
		$this->view ()->assign ( $this->_data );
		return $this->view ()->display ( $tpl, $return, $is_tpl );
	}
	
	// 判断是否是数据提交
	protected function isPost() {
		return $_SERVER ['REQUEST_METHOD'] == 'POST';
	}
	
	// 直接跳转
	protected function redirect($url, $code = 302) {
		header ( 'location:' . $url, true, $code );
		exit ();
	}
	
	// 弹出信息
	protected function alert($msg, $url = NULL) {
		header ( "Content-type: text/html; charset=utf-8" );
		$alert_msg = "alert('$msg');";
		if (empty ( $url )) {
			$gourl = 'history.go(-1);';
		} else {
			$gourl = "window.location.href = '{$url}'";
		}
		echo "<script>$alert_msg $gourl</script>";
		exit ();
	}
	
	/**
	 * ************************************************************
	 *
	 * 使用特定function对数组中所有元素做处理
	 * 
	 * @param
	 *        	string &$array 要处理的字符串
	 * @param string $function
	 *        	要执行的函数
	 * @return boolean $apply_to_keys_also 是否也应用到key上
	 * @access public
	 *        
	 *         ***********************************************************
	 */
	protected function arrayRecursive(&$array, $function, $apply_to_keys_also = false) {
		static $recursive_counter = 0;
		if (++ $recursive_counter > 1000) {
			die ( 'possible deep recursion attack' );
		}
		foreach ( $array as $key => $value ) {
			if (is_array ( $value )) {
				self::arrayRecursive ( $array [$key], $function, $apply_to_keys_also );
			} else {
				if(strstr($value,"\n")){
					$value = str_replace("\n", "\\n", $value);
				}
				
				if(strstr($value,"\"")){
					$value = str_replace("\"", "\\\"", $value);
				}
				
				//var_dump($value)."<br>";
				$array [$key] = $function ( $value );
			}
			if ($apply_to_keys_also && is_string ( $key )) {
				$new_key = $function ( $key );
				if ($new_key != $key) {
					$array [$new_key] = $array [$key];
					unset ( $array [$key] );
				}
			}
		}
		$recursive_counter --;
	}
	
	/**************************************************************
	 *
	* 将数组转换为JSON字符串（兼容中文）
	* @param array $array 要转换的数组
	* @return string 转换得到的json字符串
	* @access public
	*
	*************************************************************/
	protected function JSON($array) {
		/*echo "<pre>";
		print_r($array);
		echo "</pre>";exit;*/
		
		self::arrayRecursive ( $array, 'urlencode', true );
		$json = json_encode ( $array );
		return urldecode ( $json );
	}
	
	protected function check_user_auth($openid,$mac){
		$user_info = $this->model->table("member")->where(" md5(id) = '".$openid."' ")->find();
		if($mac != $user_info['mac'] ){
			$data_return_array ['result'] = "911";
			$data_return_array ['msg'] = "";
			$data_return_array ['data'] = "";
				
					
			$data_return = self::JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
	}
	/* 二维数组排序
	 * @param array $rowset 要排序的数组
	 * @param array $args 排序的键（array('insert_time'=>SORT_DESC)）
	 */
	public function sortByMultiCols($rowset, $args) {
		$sortArray = array();
		$sortRule = '';
		if($args){
		foreach ($args as $sortField => $sortDir) {
			if($rowset){
				foreach ($rowset as $offset => $row) {
					$sortArray[$sortField][$offset] = $row[$sortField];
				}
			}
				$sortRule .= '$sortArray[\'' . $sortField . '\'], ' . $sortDir . ', ';
			}
		}
		if (empty($sortArray) || empty($sortRule)) {
			return $rowset;
		}
		eval('array_multisort(' . $sortRule . '$rowset);');
		return $rowset;
	}
	
	public function check_token($token){
		
		$user_info=$this->model->table("member")->where(" user_token = '".$token."' ")->find();
		if(!$user_info){
			$data_return_array['result']="0";
			$data_return_array['msg']="Hack Error!";
			$data_return_array['data']="";
				
			$data_return=$this->JSON($data_return_array);
			die($data_return);
			exit();
		}else{
			$_SESSION['uuser'] = $user_info;
		}
	}
	public function i_array_column($input, $columnKey, $indexKey=null){
		if(!function_exists('array_column')){
			$columnKeyIsNumber  = (is_numeric($columnKey))?true:false;
			$indexKeyIsNull            = (is_null($indexKey))?true :false;
			$indexKeyIsNumber     = (is_numeric($indexKey))?true:false;
			$result                         = array();
			foreach((array)$input as $key=>$row){
				if($columnKeyIsNumber){
					$tmp= array_slice($row, $columnKey, 1);
					$tmp= (is_array($tmp) && !empty($tmp))?current($tmp):null;
				}else{
					$tmp= isset($row[$columnKey])?$row[$columnKey]:null;
				}
				if(!$indexKeyIsNull){
					if($indexKeyIsNumber){
						$key = array_slice($row, $indexKey, 1);
						$key = (is_array($key) && !empty($key))?current($key):null;
						$key = is_null($key)?0:$key;
					}else{
						$key = isset($row[$indexKey])?$row[$indexKey]:0;
					}
				}
				$result[$key] = $tmp;
			}
			return $result;
		}else{
			return array_column($input, $columnKey, $indexKey);
		}
	}
	public function special_filter($string)
	{
		if(!$string) return '';
		$new_string = '';
		for($i =0; isset($string[$i]); $i++)
		{
		$asc_code = ord($string[$i]);    //得到其asc码
	
		//以下代码旨在过滤非法字符
		if($asc_code == 13){
		$new_string .= '111111';
		}
		else if($asc_code > 31 && $asc_code != 127){
		$new_string .= $string[$i];
		}
		}
		return trim($new_string);
	}
	protected function if_vip($user_id)
	{
		$info = $this->model->query("SELECT COUNT(1) cnt 
				FROM app_mg_comp a
				INNER JOIN app_mg_comp_course b ON a.comp_id = b.comp_id
				AND UNIX_TIMESTAMP( NOW( ) ) 
				BETWEEN b.from_date
				AND b.to_date
				AND b.on_off_flg =0
				WHERE a.user_id =".$user_id);
		if($info){
			foreach ($info as $key => $val){
			$if_vip=$val['cnt'];
			}
		}

		
		if($if_vip!=0){
			$if_vip=1;
		}
		return $if_vip;
	}
}