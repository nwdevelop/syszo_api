<?php
class birdMod extends commonMod {
	public function info_list() {
		$type = $_POST ['type'];
		$p_num = $_POST ['p_num'];
		$p_size = $_POST ['p_size'];
		if (empty ( $p_size )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if (empty ( $p_num )) {
			$p_num = 1;
		}
		if (empty ( $type )) {
			$type = 1;
		}
		$str = ($p_num - 1) * $p_size;
		$where = '1 ';
		if ($type == '1') {
			
			$sql = "
					 SELECT A.*,B.user_nick 
			         FROM {$this->model->pre}brid A 
			         LEFT JOIN {$this->model->pre}member B ON A.user_id = B.user_id  
			         ORDER By A.insert_time DESC 
			         LIMIT " . $str . "," . $p_size . " 
					";
			$list = $this->model->query ( $sql );
			if ($list) {
				foreach ( $list as $k => $vo ) {
					$count = $this->model->table ( "brid_comments" )->where ( "bird_id = '" . $vo ['id'] . "' " )->count ();//计算回复数量
					$list [$k] ['c_n'] = $count;
					unset ( $count );
				}
			}
		} elseif ($type == '3') {
			if ($_POST ['user_id']) {
				$sql = "
				 SELECT A.*,B.user_nick 
		         FROM {$this->model->pre}brid A 
		         LEFT JOIN {$this->model->pre}member B ON A.user_id = B.user_id 
		         WHERE  A.user_id  ='" . $_POST ['user_id'] . "' 
		         ORDER By A.insert_time DESC 
		         LIMIT " . $str . "," . $p_size . "
				";
				$list = $this->model->query ( $sql );
				if ($list) {
					foreach ( $list as $k => $vo ) {
						$count = $this->model->table ( "brid_comments" )->where ( "bird_id = '" . $vo ['id'] . "' " )->count ();//计算回复数量
						$list [$k] ['c_n'] = $count;
						unset ( $count );
					}
				}
			} else {
				
				$list1 = "";
			}
		} elseif ($type == '2') {
			if ($_POST ['user_id']) {
				$friend = $this->model->table ( "fans" )->where ( "uid_a = '" . $_POST ['user_id'] . "'  " )->select ();
				
				$arr = array ();
				if($friend){
				foreach ( $friend as $key => $val ) {
					$arr [] = $val ['user_id'];
				}
				}
				$b_id = implode ( ",", $arr );
				if(!empty($b_id)){
				$sql_f = " SELECT k.*,m.user_nick FROM app_brid as k 
						   LEFT JOIN app_member AS m ON k.user_id = m.user_id 
						   WHERE k.user_id in ($b_id) 
							 ORDER By k.insert_time DESC
						   LIMIT " . $str . "," . $p_size . " 
						   ";
				$list = $this->model->query ( $sql_f );
				if ($list) {
					foreach ( $list as $k => $vo ) {
						$count = $this->model->table ( "brid_comments" )->where ( "bird_id = '" . $vo ['id'] . "' " )->count ();//计算回复数量
						$list [$k] ['c_n'] = $count;
						unset ( $count );
					}
				}
				}
			} else {
				$list1 = "";
			}
		}
		if ($_POST ['keyword']) {
			$sql = "
				 SELECT A.*,B.user_nick 
		         FROM {$this->model->pre}brid A  
		         LEFT JOIN {$this->model->pre}member B ON A.user_id = B.user_id 
		         WHERE  A.content like '%" . $_POST ['keyword'] . "%'  
		         ORDER By A.insert_time DESC 
		         LIMIT " . $str . "," . $p_size . " 
				";
			$list = $this->model->query ( $sql );
		}
		$tmp = array ();
		if ($list) {
			foreach ( $list as $key => $val ) {
				$tmp [$key] ['bird_id'] = $val ['id'];
				//$tmp [$key] ['content'] = $this->special_filter($val ['content']);
				$tmp [$key] ['content'] = $val ['content'];
				$tmp [$key] ['user_name'] = $val ['user_nick'];
				$tmp [$key] ['time'] = date ( "Y-m-d H:i", $val ['insert_time'] );
				$tmp [$key] ['user_id'] = $val ['user_id'];
				$tmp [$key] ['vip_flg'] = $this->if_vip($val ['user_id']);   //0:普通用户,1:企业用户(VIP)
				$tmp [$key] ['good_sum'] = $this->model->table ( "good" )->where (" info_id =  '" . $val ['id'] . "' and c_id = '0' and type = '2' " )->count ();
				$tmp [$key] ['comment_sum'] = $val ['c_n'];
			}
		}
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function add() {
		$user_id = $_POST ['user_id'];
		$content = $_POST ['content'];
		$img = $_POST ['img'];
		if (empty ( $user_id ) || empty ( $content )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		
		$data = array (
				"content" => $content,
				"insert_time" => time (),
				"pic" => $img,
				"user_id" => $user_id 
		);
		$this->model->table ( "brid" )->data ( $data )->insert ();
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 编辑
	public function edit() {
		$user_id = $_POST ['user_id'];
		$bird_id = $_POST ['bird_id'];
		$content = $_POST ['content'];
		$img = $_POST ['img'];
		if (empty ( $user_id ) || empty ( $content ) || empty ( $bird_id )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		//$this->special_filter($content);
		$data = array (
				"content" => $content,
				"insert_time" => time (),
				"pic" => $img,
				"user_id" => $user_id 
		);
		$this->model->table ( "brid" )->where ( "user_id = $user_id and id = $bird_id" )->data ( $data )->update ();
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//鸟删除
	public function del(){
		$user_id = $_POST['user_id'];
		$bird_id = $_POST['bird_id'];
		if (empty ( $user_id ) || empty($bird_id)) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$this->model->table('brid')->where(" id = $bird_id and user_id = $user_id")->delete();
		$this->model->table('brid_comments')->where(" bird_id = $bird_id")->delete();
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	
	public function info() {
		$user_id = $_POST ['user_id'];
		$bird_id = $_POST ['bird_id'];
		if (empty ( $bird_id )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		
		$info = $this->model->table ( "brid" )->where ( "id = '" . $bird_id . "' " )->find ();
		if ($info) {
			$user = $this->model->table ( "member" )->where ( "user_id = '" . $info ['user_id'] . "' " )->find ();
			if ($user_id) {
				$good = $this->model->table ( "good" )->where ( "user_id = '" . $user_id . "' and info_id = '" . $bird_id . "' and c_id = '0' and type = '2'" )->find ();
				if ($good) {
					$data ['if_good'] = "2";
				} else {
					$data ['if_good'] = "1";
				}
			}else{
				$data['if_good'] = "1";//如果没有登录用户，默认没赞过
			}
		}
		$data ['user_id'] = $info ['user_id'];
		$data ['vip_flg'] = $this->if_vip($info ['user_id']);   //0:普通用户,1:企业用户(VIP)
		$data ['user_name'] = $user ['user_nick'];
		$data ['time'] = date ( "Y-m-d H:i", $info ['insert_time'] );
		//$data ['content'] = $this->special_filter($info ['content']);
		$data ['content'] = $info ['content'];
		$data ['title'] = $info ['title'];
		$data ['pic'] = $info ['pic'];
		$data ['good_sum'] = $this->model->table ( "good" )->where (" info_id =  '" . $bird_id . "' and c_id = '0' and type = '2' " )->count ();
		$data ['comment_sum'] = $this->model->table ( "brid_comments" )->where ( "  bird_id = '".$bird_id."' " )->count ();

		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $data;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 鸟评论接口
	public function bird_comments() {
		$user_id = $_POST ['user_id'];
		$bird_id = $_POST ['bird_id'];
		$reply_content = $_POST ['reply_content'];
		
		if (empty ( $user_id ) || empty ( $bird_id ) || empty ( $reply_content )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		
		$data = array (
				"user_id" => $user_id,
				"bird_id" => $bird_id,
				"content" => $reply_content,
				"insert_time" => time () 
		);
		$com_id = $this->model->table ( "brid_comments" )->data ( $data )->insert ();
		if ($com_id) {
			$com_sum = $this->model->table ( 'brid' )->where ( "id = $bird_id" )->find ();
			$data = array (
					"comments_sum" => $com_sum ['comments_sum'] + "1" 
			);
			$this->model->table ( 'brid' )->where ( "id = $bird_id" )->data ( $data )->update ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 鸟评论列表
	public function bird_comments_list() {
		$bird_id = $_POST ['bird_id'];
		$p_size = $_POST ['p_size'];
		$p_num = $_POST ['p_num'];
		$user_id = $_POST ['user_id'];
		if (empty ( $bird_id ) || empty ( $p_size )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if (empty ( $p_num )) {
			$p_num = 1;
		}
		$str = ($p_num - 1) * $p_size;
		$list = $this->model->table ( "brid_comments" )->limit ( "$str" . "," . "$p_size" )->where ( " bird_id = '" . $bird_id . "' " )->order ( "insert_time asc" )->select ();
		// print_r($list);exit;
		$tmp = array ();
		// print_r($list);exit;
		if ($list) {
			foreach ( $list as $key => $val ) {
				$tmp [$key] ['user_id'] = $val ['user_id'];
				$tmp [$key] ['vip_flg'] = $this->if_vip($val ['user_id']);   //0:普通用户,1:企业用户(VIP)
				$user = $this->model->table ( "member" )->where ( "user_id  = '" . $val ['user_id'] . "' " )->find ();
				$tmp [$key] ['user_name'] = $user ['user_nick'];
				$tmp [$key] ['comments_id'] = $val ['id'];
				$tmp [$key] ['comments_time'] = date ( "Y-m-d H:i", $val ['insert_time'] );
				$tmp [$key] ['comments'] = $val ['content'];

				//コメントのいいね　数量を取得する
				$comment_goods = $this->model->table ( "good" )->where ( "info_id =  '" . $bird_id . "' and c_id = '" . $val ['id'] . "' and type = '2' " )->count ();
				$tmp [$key] ['comment_goods'] = $comment_goods;//该评论的点赞数

				if ($_POST ['user_id']) {
					$good = $this->model->table ( "good" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $bird_id . "' and c_id = '" . $val ['id'] . "' and type = '2' " )->find ();
					// print_r($good);exit;
					if ($good) {
						$tmp [$key] ['if_good'] = "2";
					} else {
						$tmp [$key] ['if_good'] = "1";
					}
				} else {
					$tmp [$key] ['if_good'] = "1";
				}
				
				if ($_POST ['user_id']) {
					$if_comment = $this->model->table ( "brid_comments" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and bird_id =  '" . $bird_id . "' " )->find ();
					if ($if_comment) {
						$tmp [$key] ['if_comment'] = "2";
					} else {
						$tmp [$key] ['if_comment'] = "1";
					}
				} else {
					$tmp [$key] ['if_comment'] = "1";
				}
				
				if ($_POST ['user_id']) {
					$if_myself = $this->model->table ( "brid" )->where ( "id  = '" . $val ['bird_id'] . "' and user_id  = '" . $_POST ['user_id'] . "' " )->find ();
					if ($if_myself) {
						$tmp [$key] ['if_myself'] = "2";
					} else {
						$tmp [$key] ['if_myself'] = "1";
					}
				} else {
					$tmp [$key] ['if_myself'] = "1";
				}
			}
		}
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 评论编辑
	public function comments_edit() {
		if (empty ( $_POST ['user_id'] ) || empty ( $_POST ['bird_id'] ) || empty ( $_POST ['content'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$list = $this->model->table ( "brid_comments" )->where ( " user_id = '" . $_POST ['user_id'] . "' " )->find ();
		if ($list) {
			$data = array (
					"content" => $_POST ['content'],
					"insert_time" => time () 
			);
			$this->model->table ( 'brid_comments' )->data ( $data )->where ( " user_id = '" . $_POST ['user_id'] . "' and id = '" . $_POST ['bird_id'] . "' " )->update ();
		} else {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "评论不存在";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	
	// 评论删除
	public function comments_del() {
		if (empty ( $_POST ['user_id'] ) || empty ( $_POST ['bird_id'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$list = $this->model->table ( "brid_comments" )->where ( " user_id = '" . $_POST ['user_id'] . "' " )->find ();
		if ($list) {
			$this->model->table ( 'brid_comments' )->where ( " user_id = '" . $_POST ['user_id'] . "' and id = '" . $_POST ['bird_id'] . "' " )->delete ();
		} else {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "该评论不存在";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function good() {
		if (empty ( $_POST ['bird_id'] ) || empty ( $_POST ['user_id'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$good = $this->model->table ( "good" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $_POST ['bird_id'] . "' and c_id = '0' and type = '2'" )->find ();
		if ($good) {
			$this->model->table ( "good" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $_POST ['bird_id'] . "' and c_id = '0' and type = '2'" )->delete ();
		} else {
			$data = array (
					"user_id" => $_POST ['user_id'],
					"info_id" => $_POST ['bird_id'],
					"c_id" => '0',
					"insert_time" => time (),
					"type" => "2" 
			);
			$this->model->table ( "good" )->data ( $data )->insert ();
		}
		/*
		 * $good = $this->model->table ( "good" )->where ( "user_id = '" . $_POST ['user_id'] . "' " )->find ();
		 *
		 * $data = array (
		 * "user_id" => $_POST ['user_id'],
		 * "bird_id" => $_POST ['bird_id'],
		 * "c_id" => $_POST ['comments_id'],
		 * "insert_time" => time (),
		 * "type" => "2"
		 * );
		 * $this->model->table ( "good" )->data ( $data )->insert ();
		 */
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function good_comments() {
		if (empty ( $_POST ['bird_id'] ) || empty ( $_POST ['user_id'] ) || empty ( $_POST ['comments_id'] )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$good = $this->model->table ( "good" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $_POST ['bird_id'] . "' and c_id = '" . $_POST ['comments_id'] . "' and type = '2'" )->find ();
		if ($good) {
			$this->model->table ( "good" )->where ( "user_id  = '" . $_POST ['user_id'] . "' and info_id =  '" . $_POST ['bird_id'] . "' and c_id = '" . $_POST ['comments_id'] . "' and type = '2'" )->delete ();
		} else {
			$data = array (
					"user_id" => $_POST ['user_id'],
					"info_id" => $_POST ['bird_id'],
					"c_id" => $_POST ['comments_id'],
					"insert_time" => time (),
					"type" => "2" 
			);
			$this->model->table ( "good" )->data ( $data )->insert ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function index() {
		// echo date("Y-m",time());
		$list = $this->diffdate ( "2014-09", date ( "Y-m", time () ) );
		rsort ( $list );
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $list;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function diffdate($date1, $date2) {
		if (strtotime ( $date1 ) > strtotime ( $date2 )) {
			$ymd = $date2;
			$date2 = $date1;
			$date1 = $ymd;
		}
		list ( $y1, $m1, $d1 ) = explode ( '-', $date1 );
		list ( $y2, $m2, $d2 ) = explode ( '-', $date2 );
		$math = ($y2 - $y1) * 12 + $m2 - $m1;
		$my_arr = array ();
		if ($y1 == $y2 && $m1 == $m2) {
			if ($m1 < 10) {
				$m1 = intval ( $m1 );
				$m1 = '0' . $m1;
			}
			if ($m2 < 10) {
				$m2 = intval ( $m2 );
				$m2 = '0' . $m2;
			}
			$my_arr [] = $y1 . $m1;
			$my_arr [] = $y2 . $m2;
			return $my_arr;
		}
		
		$p = $m1;
		$x = $y1;
		
		for($i = 0; $i <= $math; $i ++) {
			if ($p > 12) {
				$x = $x + 1;
				$p = $p - 12;
				if ($p < 10) {
					$p = intval ( $p );
					$p = '0' . $p;
				}
				$my_arr [] = $x . $p;
			} else {
				if ($p < 10) {
					$p = intval ( $p );
					$p = '0' . $p;
				}
				$my_arr [] = $x . $p;
			}
			$p = $p + 1;
		}
		return $my_arr;
	}
}