<?php if (!defined('CANPHP')) exit;?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>无标题文档</title>
<link href="<?php echo __PUBLIC__; ?>/css/styles.css" rel="stylesheet" type="text/css">
<script src="<?php echo __PUBLIC__; ?>/js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__; ?>/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#cn").click(function(e) {
      $("#yx").css("display","block");  
	  $("#cn").remove();
	  $("#bcn").css("display","none");
	  $("#byx").css("display","none");
    });
    $("#bcn").click(function(e) {
      $("#byx").css("display","block");  
	  $("#cn").remove();
	  $("#bcn").css("display","none");
	  $("#yx").css("display","none");
    });
});
</script>

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
</head>

<body>
<div class="dzsk">
  <div class="leihang">
    <p style="width:14%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:12px; color:#000; float:left; margin:5px 0 0 0;">注文№</p>
    <p style="width:auto; max-width:76%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:12px; color:#000; border-bottom:1px solid #000; float:left; margin:5px 0 0 0;"><?php echo $info['id'];?></p>
  </div>
  <div class="leihang">
    <p style="width:14%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:12px; color:#000; float:left; margin:5px 0 0 0;">発行日</p>
    <p style="width:auto; max-width:76%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:12px; color:#000; border-bottom:1px solid #000; float:left; margin:5px 0 0 0;"><?php if(!empty($info['update_time'])){ echo date("Y/m/d",$info['insert_time']);}?></p>
  </div>
  <div class="leihang" style="height:3em; margin:10px 3%;">
    <p style="width:50%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:16px; color:#000; border-bottom:1px solid #000; float:left; margin:5px 0 0 0;"><?php echo $info['user_name'];?></p>
    <p style="width:14%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:16px; color:#000; float:left; margin:5px 0 0 0;">様</p>
    <p style="width:100%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:12px; color:#000; float:left; margin:0;">下記のとおり注文致します。</p>
  </div>
  <div class="leihang" style="height:3.5em; margin:0 3% 10px 3%;">
    <p style="width:100%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:14px; color:#000; float:left; margin:0;"><?php echo $info['name'];?></p>
    <p style="width:100%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:14px; color:#000; float:left; margin:0;">〒<?php echo $info['publisher_code'];?></p>
    <p style="width:100%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:14px; color:#000; float:left; margin:0;"><?php echo $info['home'];?></p>
  </div>
  <div class="leihang" style="height:3em; margin:10px 3%;">
    <p style="width:100%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:14px; color:#000; float:left; margin:0;">TEL <?php echo $info['publisher_tel'];?></p>
    <p style="width:100%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:14px; color:#000; float:left; margin:0;">FAX <?php echo $info['publisher_fax'];?></p>
    
  </div>
  <div class="leihang" style="height:8em; margin:0 3% 10px 3%;">
    <p style="width:100%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:14px; color:#000; float:left; margin:5px 0 0 0;">[特記事項]</p>
    <font class="aaa"><?php echo $info['matter'];?></font>
    
    
  </div>
  <div class="leihang" style="height:3em;">
    <p style="width:14%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:12px; color:#000; float:left; margin:5px 0 0 0;">工期：</p>
    <p style="width:76%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:12px; color:#000; border-bottom:1px solid #000; float:left; margin:5px 0 0 0;"><?php if(!empty($info['project_time'])){ echo date("Y/m/d",$info['project_time']);}?></p>
    <p style="width:14%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:12px; color:#000; float:left; margin:5px 0 0 0;">支払日：</p>
    <p style="width:76%; font-family:'Microsoft YaHei','黑体','Arial'; font-size:12px; color:#000; border-bottom:1px solid #000; float:left; margin:5px 0 0 0;"><?php if(!empty($info['pay_time'])){ echo date("Y/m/d",$info['pay_time']);}?></p>
  </div>
  
  
  <div class="leihang" style="height:9em; margin:20px 3% 0 3%;">
    <table width="100%" border="1">
      <tr>
        <td colspan="2" style="text-align:center; background:#c0c0c0;">案件名</td>
        <td  style="text-align:center; background:#c0c0c0;">金&nbsp;&nbsp;額</td>
       
      </tr>
      <tr>
        <td colspan="2"  style="text-align:left;"><?php echo $info['title'];?></td>
        <td  style="text-align:right; background:#ccffff;"><?php echo $info['cost_without_tax'];?></td>
       
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td  style="text-align:center; background:#c0c0c0;">小　　計</td>
        <td style="text-align:right; background:#ccffff;"><?php echo $info['cost_without_tax'];?></td>
        
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td  style="text-align:center; background:#c0c0c0;">消費税等</td>
        <td style="text-align:right; background:#ccffff;"><?php echo $info['shui'];?></td>
      
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td  style="text-align:center; background:#c0c0c0;">合&nbsp;&nbsp;計</td>
        <td style="text-align:right; background:#ccffff;"><?php echo intval($info['cost_without_tax']) + $info['shui'];?></td>
      
      </tr>
    </table>
  </div>
  <div style="width:100%; height:52px; margin-top:30px;">
    <?php if($info['status'] !='2'){?>
    <center>
     <button id="cn" class="btn btn-warning" onClick="ok();">承認する</button> <button id="bcn" class="btn btn-warning" onClick="no();">承認しない</button>
     <p id="yx" style="display:none;">[承認しました。戻るボタンをタップして戻ってください。]</p>
     <p id="byx" style="display:none;">[承認を却下しました。戻るボタンをタップして戻ってください。]</p>
     </center>
     <?php }?>
  </div>
  <script>
  function ok(){
			$.post(
					 "<?php echo __APP__; ?>/message/star_order",
					 {	 
					 	id:<?php echo $info['id'];?>,
						pass_status:1
					 },
					  function (data) //回传函数
					 { 
					 	//alert(data);
						if(data == 2){
							$("#cn").hide();
							$("#bcn").hide();
						}
					 }
				 );
  			}
	function no(){
			$.post(
					 "<?php echo __APP__; ?>/message/star_order",
					 {	 
					 	id:<?php echo $info['id'];?>,
						pass_status:2
					 },
					  function (data) //回传函数
					 { 
						if(data == 2){
							$("#bcn").hide();
							$("#cn").hide();
						}
					 }
				 );
  			}
  </script>
</div>
</body>
</html>
