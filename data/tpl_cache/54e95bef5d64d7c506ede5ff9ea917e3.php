<?php if (!defined('CANPHP')) exit;?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php //echo $count;?>
<title>新闻列表</title>
<link href="<?php echo __PUBLIC__; ?>/css/styles.css" rel="stylesheet" type="text/css">
<script src="<?php echo __PUBLIC__; ?>/js/jquery.js" type="text/javascript"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
</head>

<body>
<div class="wwap">
  <ul id="page_list">
  <?php if($info_list){ foreach($info_list as $vo){ ?>
    <li onClick="a(<?php echo $vo['id']; ?>)">
      <div class="mg"><img src="<?php echo $vo['pic']; ?>" alt="<?php echo $vo['title']; ?>"></div>
      <div class="rwen">
        <div class="dat">
          <p class="rili"><?php echo $vo['insert_time']; ?></p>
          <p class="rilai">
            <a href="<?php echo __APP__; ?>/new/info_news?id=<?php echo $vo['id']; ?>" style="background-color:#da46b3"><?php echo $vo['c_name'];?></a></p>
        </div>
        <div class="nr"><?php echo $vo['content']; ?></div>
      </div>
    </li>
    <?php }}?>
  </ul>
  <?php if($page_num>1){ ?><div id="buttons" style="height: auto; line-height: auto; text-align: center;"><input type="button" onClick="next_page(this,<?php echo $page_num;?>);" style="background-color:transparent; border:0; margin-left:auto; margin-right:auto;" value="タップすると次の20件をロードします"></div><?php }?>
  <input type="hidden" id="this_page" value="1"/>
   <script>
  function next_page(obj,count){
	  //alert(obj);
	  var lis = document.getElementById("page_list").getElementsByTagName("li").length;
	 // var lis = parseInt($("#this_page").val());
	  var counts = parseInt(count-1)*20;
	  //alert(lis);
	  var next_page = parseInt($("#this_page").val())+1;
	  $.post(
			 "<?php echo __APP__; ?>/new/next_page",
			 {	 
			 	 type:1,
				 page:next_page
			 },
			 function (data) //回传函数
			{
				
				if(data != ''){
					var html ="";
					var myobj=eval('('+data+')');
					for(var i=0;i<myobj.length;i++){
					 	 html += '<li onClick=\"a('+myobj[i].id+')\">\
										<div class=\"mg\"><img src=\"'+myobj[i].pic+'" alt=\"'+myobj[i].title+'\"></div>\
											<div class=\"rwen\">\
											<div class=\"dat\">\
											<p class=\"rili\">'+myobj[i].insert_time+'</p>\
											<p class=\"rilai\">\
											  <a href=\"<?php echo __APP__; ?>/new/info_news?id='+myobj[i].id+'\" style=\"background-color\:#da46b3\">'+myobj[i].c_name+'</a></p>\
											</div>\
										<div class=\"nr\">'+myobj[i].content+'</div>\
										</div>\
    							  </li>';
						 
					}
					$("#page_list").append(html);
					$("#this_page").val(next_page);
					if(lis == counts){
						$("#buttons").hide();
					}
				}
				
			}
		);
		
  }
  
  function a(id){
	    window.location.href='<?php echo __APP__; ?>/new/info_news?id='+id;
  }
  </script>
 
</div>
</body>
</html>
